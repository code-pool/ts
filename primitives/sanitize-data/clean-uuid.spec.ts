import { Clean, NULL_UUID } from './clean'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: any,
): void {
  it(description, () =>
    expect(
      Clean.uuid(value),
    ).toEqual(expectation),
  )
}

describe('Clean.uuid', () => {
  // UUID types
  check('handles NULL when not allowed', NULL_UUID, undefined)
  it('handles NULL when allowed', () =>
    expect(
      Clean.uuid(NULL_UUID, true),
    ).toEqual(NULL_UUID),
  )

  const generalUuid = '431496e0-9ab7-4ba0-9d06-55e77dcb97db'
  const generalUuidWithoutDashes = '431496e09ab74ba09d0655e77dcb97db'
  check('handles general UUID', generalUuid, generalUuid)
  check('handles general UUID without dashes', generalUuidWithoutDashes, undefined)

  // General types
  check('handles null', null, undefined)
  check('handles undefined', undefined, undefined)

  check('handles string empty', '', undefined)
  check('handles string non-empty', 'something', undefined)
  check('handles white space', whitespace, undefined)

  check('handles zero', 0, undefined)
  check('handles number', 7.2, undefined)
  check('handles 0o144 as a number', 0o144, undefined)
  check('handles 0xff as number', 0xff, undefined)
  check('handles NaN', Number.NaN, undefined)
  check('handles Infinity', Number.POSITIVE_INFINITY, undefined)

  check('handles bigint', BigInt(7), undefined)

  check('handles object', {}, undefined)

  check('handles boolean true', true, undefined)
  check('handles boolean false', false, undefined)

  check('handles Symbol', Symbol(), undefined)

  check('handles Date', new Date(), undefined)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, undefined)
})

