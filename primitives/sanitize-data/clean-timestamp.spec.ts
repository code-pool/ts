import { Clean } from './clean'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: any,
): void {
  it(description, () =>
    expect(
      Clean.timestamp(value),
    ).toEqual(expectation),
  )
}

describe('Clean.timestamp', () => {
  // Timestamp types
  const now = new Date()
  check('handles Date object', now, now)

  const test1Value = '2022-12-31T01:23:45'
  const testDate1 = new Date(test1Value)
  check('handles ISO-8601 format', test1Value, testDate1)

  const test2Value = '1969-12-31T1:23:45'
  check('handles bad ISO-8601 format', test2Value, undefined)

  // General types
  check('handles null', null, undefined)
  check('handles undefined', undefined, undefined)

  check('handles string empty', '', undefined)
  check('handles string non-empty', 'something', undefined)
  check('handles white space', whitespace, undefined)

  check('handles zero', 0, new Date(0))
  check('handles number', 7.2, new Date(7.2))
  check('handles 0o144 as a number', 0o144, new Date(0o144))
  check('handles 0xff as number', 0xff, new Date(255))
  check('handles NaN', Number.NaN, undefined)
  check('handles Infinity', Number.POSITIVE_INFINITY, undefined)

  check('handles bigint', BigInt(7), undefined)

  check('handles object', {}, undefined)

  check('handles boolean true', true, undefined)
  check('handles boolean false', false, undefined)

  check('handles Symbol', Symbol(), undefined)

  // check('handles Date', new Date(), undefined)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, undefined)
})

