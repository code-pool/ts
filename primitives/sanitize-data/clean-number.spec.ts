import { Clean } from './clean'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: any,
): void {
  it(description, () =>
    expect(
      Clean.number(value),
    ).toEqual(expectation),
  )
}

describe('Clean.number', () => {
  // General types
  check('handles null', null, undefined)
  check('handles undefined', undefined, undefined)

  check('handles string empty', '', undefined)
  check('handles string non-empty', 'something', undefined)
  check('handles white space', whitespace, undefined)

  check('handles zero', 0, 0)
  check('handles number', 7.2, 7.2)
  check('handles 0o144 as a number', 0o144, 100)
  check('handles 0xff as number', 0xff, 255)
  check('handles NaN', Number.NaN, undefined)
  check('handles Infinity', Number.POSITIVE_INFINITY, undefined)

  check('handles bigint', BigInt(7), 7)

  check('handles object', {}, undefined)

  check('handles boolean true', true, undefined)
  check('handles boolean false', false, undefined)

  check('handles Symbol', Symbol(), undefined)

  check('handles Date', new Date(), undefined)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, undefined)

  // Other number forms
  check('handles -10 as a string', '-10', -10)
  check('handles +10 as a string', '+10', 10)
  check('handles 0o144 as a string', '0o144', 100)
  check('handles 0 as a string', '0', 0)
  check('handles 0xff as a string', '0xFF', 255)
  check('handles 8e5 as a string', '8e5', 800_000)
  check('handles 3.1415 as a string', '3.1415', 3.1415)
  check('handles -0x42', '-0x42', undefined)
  check('handles 7.2asdf', '7.2asdf', undefined)
  check('handles number with whitespace', '   \t\r\n -10    \t \r \n  ', -10)
})

