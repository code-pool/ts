import { compareValues } from './compare-values'

/**
 * For sorting strings.
 */
export const compareStrings =
  (a: string, b: string): -1 | 0 | 1 => compareValues<string>(a, b)
