import { compareValues } from './compare-values'

/** "Success" means expected value is returned. */
function check(
  description: string,
  a: string | number,
  b: string | number,
  expectation: -1 | 0 | 1,
): void {
  it(description, () => {
    expect(
      compareValues(a, b),
    ).toBe(expectation)
  })
}

describe('compareValues', () => {
  check('handles 0,0', 0, 0, 0)
  check('handles 0,1', 0, 1, -1)
  check('handles 1,0', 1, 0, 1)
  check('handles 1,1', 1, 1, 0)

  check('handles "a","a"', 'a', 'a', 0)
  check('handles "a","b"', 'a', 'b', -1)
  check('handles "b","a"', 'b', 'a', 1)
  check('handles "b","b"', 'b', 'b', 0)
})

