export interface Pojo<T = any> {
  [key: string]: T;
}
