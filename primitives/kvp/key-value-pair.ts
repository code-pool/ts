/**
 * This is a basic key:value pair.
 *
 * This was originally intended for UI elements
 * such as lists, dropdowns, tabs, etc.
 * However, it's generically useful in a number of situations.
 *
 * When used for UI elements,
 * the type parameters should be `string` or equivalent,
 * such as a string union (e.g. `'a' | 'b' | 'c'`).
 */
export interface KeyValuePair<KeyType = string, ValueType = string> {
  key: KeyType;
  value: ValueType;
}
