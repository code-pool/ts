import { SetOperators } from './set-operators'

const emptySet = new Set()
const set1 = new Set([1])
const set2 = new Set([2])
const set12 = new Set([1, 2])

const setOperator = <T>(a: Set<T>, b: Set<T>): boolean =>
  SetOperators.isSuperset(a, b)

describe('SetOperators.isSuperset', () => {
  it('{} ⊇ {} = true', () => expect(
    setOperator(emptySet, emptySet),
  ).toEqual(true))

  it('{1} ⊇ {} = true', () => expect(
    setOperator(set1, emptySet),
  ).toEqual(true))

  it('{} ⊇ {1} = false', () => expect(
    setOperator(emptySet, set1),
  ).toEqual(false))

  it('{1} ⊇ {1} = true', () => expect(
    setOperator(set1, set1),
  ).toEqual(true))

  it('{1} ⊇ {2} = false', () => expect(
    setOperator(set1, set2),
  ).toEqual(false))

  it('{2} ⊇ {1} = false', () => expect(
    setOperator(set2, set1),
  ).toEqual(false))

  it('{} ⊇ {1,2} = false', () => expect(
    setOperator(emptySet, set12),
  ).toEqual(false))

  it('{1,2} ⊇ {} = true', () => expect(
    setOperator(set12, emptySet),
  ).toEqual(true))

  it('{1} ⊇ {1,2} = false', () => expect(
    setOperator(set1, set12),
  ).toEqual(false))

  it('{1,2} ⊇ {1} = true', () => expect(
    setOperator(set12, set1),
  ).toEqual(true))

  it('{1,2} ⊇ {1,2} = true', () => expect(
    setOperator(set12, set12),
  ).toEqual(true))
})
