/**
 * JavaScript `Set` is native to the language, but there are no
 * built-in set operators.
 *
 * Adapted from MDN documentation for Set()
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
 */
export class SetOperators {
  /**
   * @return Set<T>: {A} - {B} is all elements of {A} not in {B}
   */
  public static difference<T>(A: Set<T>, B: Set<T>): Set<T> {
    const difference = new Set(A)
    // eslint-disable-next-line no-restricted-syntax
    for (const elem of B) {
      difference.delete(elem)
    }
    return difference
  }

  /**
   * @return boolean: {A} = {}
   */
  public static empty<T>(A: Set<T>): boolean {
    return A.size === 0
  }

  /**
   * @return boolean: {A} = {B}
   */
  public static equals<T>(A: Set<T>, B: Set<T>): boolean {
    // If two sets have the same number of elements, and
    // one is the superset of the other, then the sets are equal.
    return A.size === B.size && SetOperators.isSuperset(A, B)
  }

  /**
   * @return Set<T>: {A} ∩ {B}
   */
  public static intersection<T>(A: Set<T>, B: Set<T>): Set<T> {
    const intersection = new Set<T>()
    // eslint-disable-next-line no-restricted-syntax
    for (const elem of B) {
      if (A.has(elem)) {
        intersection.add(elem)
      }
    }
    return intersection
  }

  /**
   * @return boolean: {A} ⊇ {B} if {A} contains all elements of {B}.
   */
  public static isSuperset<T>(A: Set<T>, B: Set<T>): boolean {
    // eslint-disable-next-line no-restricted-syntax
    for (const elem of B) {
      if (!A.has(elem)) {
        return false
      }
    }
    return true
  }

  /**
   * @return Set<T>: {A} △ {B} is all elements not common to {A} and {B} (XOR).
   */
  public static symmetricDifference<T>(A: Set<T>, B: Set<T>): Set<T> {
    const difference = new Set(A)
    // eslint-disable-next-line no-restricted-syntax
    for (const elem of B) {
      if (difference.has(elem)) {
        difference.delete(elem)
      } else {
        difference.add(elem)
      }
    }
    return difference
  }

  /**
   * @return Set<T>: {A} ∪ {B}
   */
  public static union<T>(A: Set<T>, B: Set<T>): Set<T> {
    const union = new Set(A)
    // eslint-disable-next-line no-restricted-syntax
    for (const elem of B) {
      union.add(elem)
    }
    return union
  }
}
