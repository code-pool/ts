import { toString } from './to-string'

const whitespace = '    \t\r\n  '

/** "Success" means default value is returned. */
function checkDefault(
  description: string,
  value: any,
  defaultValue: string,
): void {
  it(description, () => {
    expect(
      toString(value, defaultValue),
    ).toBe(defaultValue)
  })
}

/** "Success" means expected value is returned. */
function check(
  description: string,
  value: any,
  expectation: any,
): void {
  it(description, () =>
    expect(
      toString(value),
    ).toEqual(expectation),
  )
}

describe('toString', () => {
  checkDefault('returns a value as a default', undefined, '123')
  checkDefault('returns a value as a default', null, '123')

  // General types
  check('handles null', null, '')
  check('handles undefined', undefined, '')

  check('handles string empty', '', '')
  check('handles string non-empty', 'something', 'something')
  check('handles white space', whitespace, whitespace)

  check('handles zero', 0, '0')
  check('handles number', 7.2, '7.2')
  check('handles 0o144 as a number', 0o144, '100')
  check('handles 0xff as number', 0xff, '255')
  check('handles NaN', Number.NaN, 'NaN')
  check('handles Infinity', Number.POSITIVE_INFINITY, 'Infinity')

  check('handles bigint', BigInt(7), '7')

  check('handles object', {}, '[object Object]')

  check('handles boolean true', true, 'true')
  check('handles boolean false', false, 'false')

  check('handles Symbol', Symbol(), 'Symbol()')

  const d = new Date()
  check('handles Date', d, d.toString())

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, '[function]')
})

