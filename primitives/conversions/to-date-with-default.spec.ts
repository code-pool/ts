// import { toDateWithDefault } from './to-date-with-default'
//
// const whitespace = '    \t\r\n  '
// const now = new Date()
//
// /**
//  * `Date()` constructor months are one below the common ordinal value,
//  * i.e. January = 0, February = 1, etc.
//  */
// const November = 10
//
// /**
//  * Date/times transported across APIs are assumed to be ISO-8601 in UTC.
//  */
// const tests = [
//   // JavaScript `Date` does not  handle ISO-8601 basic format.
//   {
//     string: '20221110',
//     date: null,
//   },
//   {
//     string: '2022-11-10',
//     date: new Date(Date.UTC(2022, November, 10, 0, 0, 0, 0)),
//   },
//   // JavaScript `Date` does not  handle ISO-8601 basic format.
//   {
//     string: '20221110T123456Z',
//     date: null,
//   },
//   {
//     string: '2022-11-10T12:34:56Z',
//     date: new Date(Date.UTC(2022, November, 10, 12, 34, 56, 0)),
//   },
//   // JavaScript `Date` does not  handle ISO-8601 basic format.
//   {
//     string: '20221110T123456Z',
//     date: null,
//   },
//   {
//     string: '2022-11-10T12:34:56Z',
//     date: new Date(Date.UTC(2022, November, 10, 12, 34, 56, 0)),
//   },
// ]
//
// function check(
//   description: string,
//   value: any,
//   defaultValue: Date | null | undefined,
//   expectation: any,
// ): void {
//   it(description, () =>
//     expect(
//       toDateWithDefault(value, defaultValue),
//     ).toEqual(expectation),
//   )
// }
//
describe('toDateWithDefault', () => {
  it(`isn't implemented yet`, () => expect(true).toEqual(true))
//
//   check('returns null as a default', undefined, null, null)
//   check('returns a value as a default', undefined, now, now)
//   check('respects `undefined` as a default', undefined, undefined, undefined)
//
//   // General types
//   check('handles null', null, now, now)
//   check('handles undefined', undefined, now, now)
//
//   check('handles string empty', '', now, now)
//   check('handles string non-empty', 'something', now, now)
//   check('handles white space', whitespace, now, now)
//
//   check('handles zero', 0, null, new Date(0))
//   check('handles number', now.getTime(), null, now)
//   check('handles NaN', Number.NaN, null, null)
//   check('handles Infinity', Number.POSITIVE_INFINITY, null, null)
//
//   check('handles bigint', BigInt(now.getTime()), null, now)
//
//   check('handles object', {}, null, null)
//
//   check('handles boolean true', true, null, null)
//   check('handles boolean false', false, null, null)
//
//   check('handles Symbol', Symbol(), null, null)
//
//   check('handles Date', now, null, now)
//
//   // eslint-disable-next-line @typescript-eslint/no-empty-function
//   check('handles function', () => {
//   }, null, null)
//
//   // Other date forms
//   for (const i of tests) {
//     check(`handles "${i.string}"`, i.string, null, i.date)
//   }
})

