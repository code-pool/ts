/**
 * Convert to a number.
 *
 * If conversion fails, fall back to a default value.
 * The default value can be anything.
 *
 * For example `toNumberWithDefault(x, null)`
 * will return `null` to indicate the conversion failed.
 */
export function toNumberWithDefault<T>(value: unknown, defaultValue: T): number | T {
  if (typeof value === 'number') {
    return Number.isFinite(value) ? value : defaultValue
  }

  if (typeof value === 'string') {
    const v = value.trim()
    if (v === '') {
      return defaultValue
    }

    const n = Number(value)
    return Number.isFinite(n) ? n : defaultValue
  }

  if (typeof value === 'bigint') {
    const n = Number(value);
    return Number.isFinite(n) ? n : defaultValue;
  }

  return defaultValue
}
