import { toNumberWithDefault } from './to-number-with-default'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: unknown,
  defaultValue: number | null | undefined,
  expectation: unknown,
): void {
  it(description, () =>
    expect(
      toNumberWithDefault(value, defaultValue),
    ).toEqual(expectation),
  )
}

describe('toNumberWithDefault', () => {
  check('returns null as a default', undefined, null, null)
  check('returns a value as a default', undefined, 123, 123)
  check('returns respects `undefined` as a default', undefined, undefined, undefined)

  // General types
  check('handles null', null, 123, 123)
  check('handles undefined', undefined, 123, 123)

  check('handles string empty', '', null, null)
  check('handles string non-empty', 'something', null, null)
  check('handles white space', whitespace, null, null)

  check('handles zero', 0, null, 0)
  check('handles number', 7.2, null, 7.2)
  check('handles 0o144 as a number', 0o144, null, 100)
  check('handles 0xff as number', 0xff, null, 255)
  check('handles NaN', Number.NaN, null, null)
  check('handles Infinity', Number.POSITIVE_INFINITY, null, null)

  check('handles bigint', BigInt(7), null, 7)

  check('handles object', {}, null, null)

  check('handles boolean true', true, null, null)
  check('handles boolean false', false, null, null)

  check('handles Symbol', Symbol(''), null, null)

  check('handles Date', new Date(), null, null)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, null, null)

  // Other number forms
  check('handles -10 as a string', '-10', null, -10)
  check('handles +10 as a string', '+10', null, 10)
  check('handles 0o144 as a string', '0o144', null, 100)
  check('handles 0 as a string', '0', null, 0)
  check('handles 0xff as a string', '0xFF', null, 255)
  check('handles 8e5 as a string', '8e5', null, 800_000)
  check('handles 3.1415 as a string', '3.1415', null, 3.1415)
  check('handles -0x42', '-0x42', null, null)
  check('handles 7.2asdf', '7.2asdf', null, null)
  check('handles number with whitespace', '   \t\r\n -10    \t \r \n  ', null, -10)

  // BigInt is arbitrary precision, so it can handle numbers
  // far greater than what can be handled by a double-precision float.
  // These will be converted to `Infinity`.
  let reallyBig = BigInt(Number.MAX_VALUE)
  // eslint-disable-next-line operator-assignment
  reallyBig = reallyBig * reallyBig
  check('handles really big bigint', reallyBig, null, null)
})

