/**
 * Turn a value into a string.
 * If the value is `null` or `undefined`, use the default value.
 * If no default value is provided, use an empty string for the default.
 */
export function toString(
  value: any,
  defaultValue?: string,
): string {
  if (typeof value === 'string') {
    return value
  } else if (value === null || value === undefined) {
    return defaultValue ?? ''
  } else if (typeof value === 'function') {
    return '[function]'
  } else {
    return value.toString()
  }
}
