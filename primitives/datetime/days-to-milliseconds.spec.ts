import { daysToMilliseconds } from './days-to-milliseconds'

describe('daysToMilliseconds', () => {
  it('handles -2 days correctly', () => {
    expect(daysToMilliseconds(-2)).toBe(-2 * 24 * 3600 * 1000)
  })

  it('handles -1 days correctly', () => {
    expect(daysToMilliseconds(-1)).toBe(-1 * 24 * 3600 * 1000)
  })

  it('handles -0.5 days correctly', () => {
    expect(daysToMilliseconds(-0.5)).toBe(-0.5 * 24 * 3600 * 1000)
  })

  it('handles 0 days correctly', () => {
    expect(daysToMilliseconds(0)).toBe(0)
  })

  it('handles 0.5 days correctly', () => {
    expect(daysToMilliseconds(0.5)).toBe(0.5 * 24 * 3600 * 1000)
  })

  it('handles 1 days correctly', () => {
    expect(daysToMilliseconds(1)).toBe(24 * 3600 * 1000)
  })

  it('handles 2 days correctly', () => {
    expect(daysToMilliseconds(2)).toBe(2 * 24 * 3600 * 1000)
  })

})
