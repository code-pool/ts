import { MillisecondsPerDay } from './milliseconds-per-day'

/** Convert a number of days to milliseconds. */
export function daysToMilliseconds(days: number): number {
  return days * MillisecondsPerDay
}
