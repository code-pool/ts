/**
 * Helper constant.
 * For converting between milliseconds and days,
 * prefer to use the functions `daysToMilliseconds` and
 * `millisecondsToDays`.
 */
export const MillisecondsPerDay = 24 * 3600 * 1000
