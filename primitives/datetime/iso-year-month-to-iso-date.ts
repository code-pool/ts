/**
 * Convert from YYYY-MM to YYYY-MM-DD.
 * This arbitrarily chooses the first day of the month.
 */
export function isoYearMonthToIsoDate(value: unknown): string | undefined {
  if (typeof value !== 'string') return undefined

  return value.match(/^\d{4}-\d{2}$/) ? `${value}-01` : value
}
