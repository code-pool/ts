import { toDateWithDefault } from '../conversions/to-date-with-default'

/**
 * This converts a `Date` to a YYYY-MM-DD format.
 *
 * A "day" is not compatible with "a specific point in time",
 * which is what a JavaScript `Date` object instance
 * represents.
 *
 * In RAA, a "day" gets stored as midnight,
 * i.e. YYYY-MM-DDT00:00:00Z, which is a common practice.
 *
 * However, "day" may actually be a timestamp,
 * which implies an event that happened at a specific point in time,
 * which implies a local time rather than GMT.
 *
 * For determining whether two events happen on the same "day",
 * it's convenient to compare the ISO dates (without time information).
 *
 * To handle this, the following logic is pursued:
 * 1) If the ISO representation is at midnight,
 *    i.e. it contains T00:00:00,
 *    then assume the time portion is garbage and accept the
 *    YYYY-MM-DD portion of the string as the date.
 * 2) Otherwise, assume the point in time is a timestamp in local time.
 *    Use local time to build the YYYY-MM-DD representation.
 */
export function dateToIsoDate(value: Date | string | number | undefined | null): string | null {
  const date = toDateWithDefault(value, null)
  if (!date) {
    return null
  }

  const iso = date.toISOString()
  if (iso.indexOf('T00:00:00') >= 0) {
    return iso.substring(0, 10)
  }

  const year = date.toLocaleString('en-US', { year: 'numeric' })
  const month = date.toLocaleString('en-US', { month: '2-digit' })
  const day = date.toLocaleString('en-US', { day: '2-digit' })

  return `${year}-${month}-${day}`
}
