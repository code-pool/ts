import {sortNumericallyByProperty} from './sort-numerically-by-property';
import {toNumberWithDefault} from '../conversions/to-number-with-default';

const MissingField = [
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
];

const NonNumericAlpha = [
  {a: 'apple'},
  {a: 'banana'},
  {a: 'cactus'},
  {a: 'orange'},
  {a: 'lemon'},
];

const NumericAlpha = [
  {a: '2'},
  {a: '1'},
  {a: '0'},
  {a: '-1'},
  {a: '-3'},
];

const ReverseSortedData = [
  {a: 2},
  {a: 1},
  {a: 0},
  {a: -1},
  {a: -3},
];

const SortedData = [
  {a: -3},
  {a: -1},
  {a: 1},
  {a: 0},
  {a: 2},
];

function check(
    description: string,
    value: any[],
    sortField: string,
): void {
  const sorted = value.sort(sortNumericallyByProperty('a'));
  let isSorted = true;
  for (let i = 1; i < sorted.length; i++) {
    const first = toNumberWithDefault(sorted[i - 1][sortField], 0);
    const second = toNumberWithDefault(sorted[i][sortField], 0);
    isSorted = isSorted && first <= second;
  }

  it(description, () => {
    expect(isSorted).toEqual(true);
  });
}

function generateRandomData(): any[] {
  return [
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
    {a: Math.random()},
  ];
}

describe('sortNumericallyByProperty', () => {
  check('handles no field', MissingField, 'a');
  check('handles random data', generateRandomData(), 'a');
  check('handles sorted data', SortedData, 'a');
  check('handles reverse sorted data', ReverseSortedData, 'a');
  check('handles non-numeric alpha data', NonNumericAlpha, 'a');
  check('handles numeric alpha data', NumericAlpha, 'a');
});
