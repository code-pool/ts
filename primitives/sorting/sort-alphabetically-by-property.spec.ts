import {sortAlphabeticallyByProperty} from './sort-alphabetically-by-property';
import {toString} from '../conversions/to-string';

const MissingField = [
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
];

const UnorderedArray = [
  {a: 'apple'},
  {a: 'lemon'},
  {a: 'orange'},
  {a: 'cactus'},
  {a: 'banana'},
];

const NumericArray = [
  {a: 2},
  {a: 1},
  {a: 0},
  {a: -1},
  {a: -3},
];

const ReverseSortedData = [
  {a: 'orange'},
  {a: 'lemon'},
  {a: 'cactus'},
  {a: 'banana'},
  {a: 'apple'},
];

const SortedData = [
  {a: 'apple'},
  {a: 'banana'},
  {a: 'cactus'},
  {a: 'lemon'},
  {a: 'orange'},
];

function check(
    description: string,
    value: any[],
    sortField: string,
): void {
  const sorted = value.sort(sortAlphabeticallyByProperty('a'));
  let isSorted = true;
  for (let i = 1; i < sorted.length; i++) {
    const first = toString(sorted[i - 1][sortField]);
    const second = toString(sorted[i][sortField]);
    isSorted = isSorted && first <= second;
  }

  it(description, () => {
    expect(isSorted).toEqual(true);
  });
}

function generateRandomData(): any[] {
  return [
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
    {a: Math.random().toString()},
  ];
}

describe('sortAlphabeticallyByProperty', () => {
  check('handles no field', MissingField, 'a');
  check('handles random data', generateRandomData(), 'a');
  check('handles sorted data', SortedData, 'a');
  check('handles reverse sorted data', ReverseSortedData, 'a');
  check('handles unordered data', UnorderedArray, 'a');
  check('handles numeric data', NumericArray, 'a');
});
