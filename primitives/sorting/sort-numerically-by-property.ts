import {compareNumbers} from '../comparisons/compare-numbers';
import {Pojo} from '../types/pojo';
import {toNumberWithDefault} from '../conversions/to-number-with-default';

/**
 * This is for the `sort` method to sort by a numerical field.
 *
 * Example:
 *
 * Given an array of objects with a field "orderBy",
 * return an array sorted by this field:
 *
 * const sortedArray = myArray.sort(sortNumericallyByProperty('orderBy')
 */
export function sortNumericallyByProperty<T extends Pojo>(property: string): (a: T, b: T) => number {
  return (a: T, b: T) => compareNumbers(
      toNumberWithDefault(a[property], 0),
      toNumberWithDefault(b[property], 0),
  );
}

