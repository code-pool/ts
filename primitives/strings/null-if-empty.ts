/**
 * Converts a string into null if it's empty.
 */
export function nullIfEmpty(
  value: string | null | undefined,
): string | null {
  return typeof value === 'string'
    ? (value === '' ? null : value)
    : null
}
