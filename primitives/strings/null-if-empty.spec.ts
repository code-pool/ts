import { nullIfEmpty } from './null-if-empty'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: string | null,
): void {
  it(description, () =>
    expect(
      nullIfEmpty(value),
    ).toEqual(expectation),
  )
}

describe('nullIfEmpty', () => {

  // General types
  check('handles null', null, null)
  check('handles undefined', undefined, null)

  check('handles string empty', '', null)
  check('handles string non-empty', 'something', 'something')
  check('handles white space', whitespace, whitespace)

  check('handles zero', 0, null)
  check('handles number', 7.2, null)
  check('handles 0o144 as a number', 0o144, null)
  check('handles 0xff as number', 0xff, null)
  check('handles NaN', Number.NaN, null)
  check('handles Infinity', Number.POSITIVE_INFINITY, null)

  check('handles bigint', BigInt(7), null)

  check('handles object', {}, null)

  check('handles boolean true', true, null)
  check('handles boolean false', false, null)

  check('handles Symbol', Symbol(), null)

  check('handles Date', new Date(), null)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, null)
})

