import { isNullOrEmpty } from './is-null-or-empty'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: unknown,
  expectation: boolean,
) {
  it(description, () =>
    expect(
      isNullOrEmpty(value as string),
    ).toEqual(expectation),
  )
}

describe('isNullOrEmpty', () => {

  // Expected values
  check('handles empty string', '', true)
  check('handles null', null, true)

  check('handles 1 space', ' ', false)
  check('handles 2 spaces', '  ', false)
  check('handles alphanumeric a', 'a', false)
  check('handles alphanumeric ab', 'ab', false)
  check('handles alphanumeric ab1', 'ab1', false)
  check('handles alphanumeric ab12', 'ab12', false)

  // General types
  check('handles null', null, true)
  check('handles undefined', undefined, true)

  check('handles string empty', '', true)
  check('handles string non-empty', 'something', false)
  check('handles white space', whitespace, false)

  check('handles zero', 0, true)
  check('handles number', 7.2, true)
  check('handles 0o144 as a number', 0o144, true)
  check('handles 0xff as number', 0xff, true)
  check('handles NaN', Number.NaN, true)
  check('handles Infinity', Number.POSITIVE_INFINITY, true)

  check('handles bigint', BigInt(7), true)

  check('handles object', {}, true)

  check('handles boolean true', true, true)
  check('handles boolean false', false, true)

  check('handles Symbol', Symbol(''), true)

  check('handles Date', new Date(), true)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, true)
})

