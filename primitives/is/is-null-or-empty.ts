/**
 * Tests whether a value is null or an empty string.
 * This is for cases where an empty string is considered "no data".
 *
 * @param value
 */
export function isNullOrEmpty(value: string | null | undefined): value is null {
  return typeof value !== 'string' || value === ''
}

