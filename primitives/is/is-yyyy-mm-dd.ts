/**
 * Determine whether a string is an ISO-8601 date (without time),
 * i.e. YYYY-MM-DD.
 *
 */
export function isYyyyMmDd(value: unknown): boolean {
  if (typeof value !== 'string') return false

  return /^\d{4}-?\d{2}-?\d{2}$/.test(value)
}
