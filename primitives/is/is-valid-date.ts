/**
 * Determine if the value is a `Date` object with a valid date/time.
 */
export function isValidDate(value: unknown): value is Date {
  return value instanceof Date && Number.isFinite(value.getTime()) && value.getTime() !== 0
}
