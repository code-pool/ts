import { isValidDate } from './is-valid-date'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: any,
): void {
  it(description, () =>
    expect(
      isValidDate(value),
    ).toEqual(expectation),
  )
}

describe('isValidDate', () => {

  // General types
  check('handles null', null, false)
  check('handles undefined', undefined, false)

  check('handles string empty', '', false)
  check('handles string non-empty', 'something', false)
  check('handles white space', whitespace, false)

  check('handles zero', 0, false)
  check('handles NaN', Number.NaN, false)
  check('handles Infinity', Number.POSITIVE_INFINITY, false)

  check('handles bigint', BigInt(7), false)

  check('handles object', {}, false)

  check('handles boolean true', true, false)
  check('handles boolean false', false, false)

  check('handles Symbol', Symbol(), false)

  check('handles Date', new Date(), true)
  check('handles bad Date', new Date('something'), false)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, false)
})

