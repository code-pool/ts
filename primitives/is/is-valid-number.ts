import { toNumberWithDefault } from '../conversions/to-number-with-default'

/**
 * Determine if the value represents a finite numeric value;
 * i.e. an actual real number.
 */
export function isValidNumber(value: any): boolean {
  return toNumberWithDefault(value, null) !== null
}
