import { isPojo } from './is-pojo'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: boolean,
) {
  it(description, () =>
    expect(
      isPojo(value),
    ).toEqual(expectation),
  )
}

describe('isPojo', () => {

  // General types
  check('handles null', null, false)
  check('handles undefined', undefined, false)

  check('handles string empty', '', false)
  check('handles string non-empty', 'something', false)
  check('handles white space', whitespace, false)

  check('handles zero', 0, false)
  check('handles number', 7.2, false)
  check('handles 0o144 as a number', 0o144, false)
  check('handles 0xff as number', 0xff, false)
  check('handles NaN', Number.NaN, false)
  check('handles Infinity', Number.POSITIVE_INFINITY, false)

  check('handles bigint', BigInt(7), false)

  check('handles object', {}, true)

  check('handles boolean true', true, false)
  check('handles boolean false', false, false)

  check('handles Symbol', Symbol(), false)

  check('handles Date', new Date(), true)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, false)

})

