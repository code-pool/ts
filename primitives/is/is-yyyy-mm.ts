/**
 * Determine whether a string is an ISO-8601 year and month (without day or time),
 * i.e. YYYY-MM.
 *
 */
export function isYyyyMm(value: unknown): boolean {
  if (typeof value !== 'string') return false

  return /^\d{4}-?\d{2}$/.test(value)
}
