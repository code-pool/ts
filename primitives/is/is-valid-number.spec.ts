import { isValidNumber } from './is-valid-number'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: any,
) {
  it(description, () =>
    expect(
      isValidNumber(value),
    ).toEqual(expectation),
  )
}

describe('isValidNumber', () => {

  // General types
  check('handles null', null, false)
  check('handles undefined', undefined, false)

  check('handles string empty', '', false)
  check('handles string non-empty', 'something', false)
  check('handles white space', whitespace, false)

  check('handles zero', 0, true)
  check('handles number', 7.2, true)
  check('handles 0o144 as a number', 0o144, true)
  check('handles 0xff as number', 0xff, true)
  check('handles NaN', Number.NaN, false)
  check('handles Infinity', Number.POSITIVE_INFINITY, false)

  check('handles bigint', BigInt(7), true)

  check('handles object', {}, false)

  check('handles boolean true', true, false)
  check('handles boolean false', false, false)

  check('handles Symbol', Symbol(), false)

  check('handles Date', new Date(), false)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, false)

  // Other number forms
  check('handles -10 as a string', '-10', true)
  check('handles +10 as a string', '+10', true)
  check('handles 0o144 as a string', '0o144', true)
  check('handles 0 as a string', '0', true)
  check('handles 0xff as a string', '0xFF', true)
  check('handles 8e5 as a string', '8e5', true)
  check('handles 3.1415 as a string', '3.1415', true)
  check('handles -0x42', '-0x42', false)
  check('handles 7.2asdf', '7.2asdf', false)
  check('handles number with whitespace', '   \t\r\n -10    \t \r \n  ', true)
})

