import { isNullish } from './is-nullish'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: boolean,
) {
  it(description, () =>
    expect(
      isNullish(value),
    ).toEqual(expectation),
  )
}

describe('util.isNullish', () => {

  // General types
  check('handles null', null, true)
  check('handles undefined', undefined, true)

  check('handles string empty', '', false)
  check('handles string non-empty', 'something', false)
  check('handles white space', whitespace, false)

  check('handles zero', 0, false)
  check('handles number', 7.2, false)
  check('handles 0o144 as a number', 0o144, false)
  check('handles 0xff as number', 0xff, false)
  check('handles NaN', Number.NaN, false)
  check('handles Infinity', Number.POSITIVE_INFINITY, false)

  check('handles bigint', BigInt(7), false)

  check('handles object', {}, false)

  check('handles boolean true', true, false)
  check('handles boolean false', false, false)

  check('handles Symbol', Symbol(), false)

  check('handles Date', new Date(), false)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, false)
})

