import { isComparableType } from './is-comparable-type'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: boolean,
) {
  it(description, () =>
    expect(
      isComparableType(value),
    ).toEqual(expectation),
  )
}

describe('isComparableType', () => {

  // General types
  check('handles null', null, true)
  check('handles undefined', undefined, true)

  check('handles string empty', '', true)
  check('handles string non-empty', 'something', true)
  check('handles white space', whitespace, true)

  check('handles zero', 0, true)
  check('handles number', 7.2, true)
  check('handles 0o144 as a number', 0o144, true)
  check('handles 0xff as number', 0xff, true)
  check('handles NaN', Number.NaN, true)
  check('handles Infinity', Number.POSITIVE_INFINITY, true)

  check('handles bigint', BigInt(7), true)

  check('handles object', {}, false)

  check('handles boolean true', true, true)
  check('handles boolean false', false, true)

  check('handles Symbol', Symbol(), true)

  check('handles Date', new Date(), false)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, true)

})

