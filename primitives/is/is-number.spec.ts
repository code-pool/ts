import { isNumber } from './is-number'

const whitespace = '    \t\r\n  '

function check(
  description: string,
  value: any,
  expectation: boolean,
) {
  it(description, () =>
    expect(
      isNumber(value),
    ).toEqual(expectation),
  )
}

describe('isNumber', () => {

  // General types
  check('handles null', null, false)
  check('handles undefined', undefined, false)

  check('handles string empty', '', false)
  check('handles string non-empty', 'something', false)
  check('handles white space', whitespace, false)

  check('handles zero', 0, true)
  check('handles number', 7.2, true)
  check('handles 0o144 as a number', 0o144, true)
  check('handles 0xff as number', 0xff, true)
  check('handles NaN', Number.NaN, true)
  check('handles Infinity', Number.POSITIVE_INFINITY, true)

  check('handles bigint', BigInt(7), false)

  check('handles object', {}, false)

  check('handles boolean true', true, false)
  check('handles boolean false', false, false)

  check('handles Symbol', Symbol(), false)

  check('handles Date', new Date(), false)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  check('handles function', () => {
  }, false)

  // Other number forms
  check('handles -10 as a string', '-10', false)
  check('handles +10 as a string', '+10', false)
  check('handles 0o144 as a string', '0o144', false)
  check('handles 0 as a string', '0', false)
  check('handles 0xff as a string', '0xFF', false)
  check('handles 8e5 as a string', '8e5', false)
  check('handles 3.1415 as a string', '3.1415', false)
  check('handles -0x42', '-0x42', false)
  check('handles 7.2asdf', '7.2asdf', false)
  check('handles number with whitespace', '   \t\r\n -10    \t \r \n  ', false)
})

