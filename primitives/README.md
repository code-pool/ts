# Primitives

This is a collection of simple functions that deal with primitives and
built-ins. Full testing accompanies these files.

The intent is to keep this dependency-free and portable.
